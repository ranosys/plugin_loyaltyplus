'use strict';

var base = module.superModule;
var system = require('dw/system');
var site = system.Site.getCurrent();
var loyaltyHelper = require('*/cartridge/scripts/helpers/loyaltyHelpers');

/**
 *
 * @param {Object} lineItemContainer current basket
 * @returns {string} points in amount to be reddemed
 */
function getRedeemablePoints(lineItemContainer) {
    var profile = lineItemContainer.customer.profile;
    var availablePoints = profile && profile.custom && profile.custom.loyaltyTotalPoints ? profile.custom.loyaltyTotalPoints : 0;
    var subtotal = 0;
    var productLineItems = lineItemContainer.getAllProductLineItems();
    for (var i = 0; i < productLineItems.length; i++) {
        var productLineItem = productLineItems[i];
        subtotal += productLineItem.adjustedPrice.value;
    }
    var redemptionLimitPercentage = site.getCustomPreferenceValue('redemptionLimitPercentage');
    var pointsToDollarsConversionRate = site.getCustomPreferenceValue('PointsToDollarsConversionRate');
    var redemptionValue = (subtotal * (redemptionLimitPercentage / 100));
    if (redemptionValue > availablePoints / pointsToDollarsConversionRate) {
        redemptionValue = availablePoints / pointsToDollarsConversionRate;
    }
    var redeemablePoints = redemptionValue * pointsToDollarsConversionRate;
    return Math.round(redeemablePoints).toString();
}

/**
 * Order class that represents the current order
 * @param {dw.order.LineItemCtnr} lineItemContainer - Current users's basket/order
 * @param {Object} options - The current order's line items
 * @param {Object} options.config - Object to help configure the orderModel
 * @param {string} options.config.numberOfLineItems - helps determine the number of lineitems needed
 * @param {string} options.countryCode - the current request country code
 * @constructor
 */
function OrderModel(lineItemContainer, options) {
    base.call(this, lineItemContainer, options);
    // eslint-disable-next-line no-undef
    var customer = request.session.customer;
    if (loyaltyHelper.isOrderFromValidMember(customer)) {
        this.redeemablePoints = getRedeemablePoints(lineItemContainer);
        this.isLoyaltyRedemption = 'custom' in lineItemContainer && 'isLoyaltyRedemption' in lineItemContainer.custom && lineItemContainer.custom.isLoyaltyRedemption;
    }
}

module.exports = OrderModel;

/**
 * @File Name          : account.js
 * @Description        : account.Js file extended from default SFRA to integrate loyalty field in customer profile
 * @Modification
 * Ver       Modification
 * 1.0    Changes regarding loyalty field in customer profile (function:getProfile)
*/

'use strict';

var base = module.superModule;

/**
 *
 * @param {Object} profile profile object of customer
 * @param {Object} rawCustomerProfileObject raw customer object
 * @returns {Object} an object that contains information about the current customer's profile
 */
function getProfile(profile, rawCustomerProfileObject) {
    var result;
    var isLoyaltyMember = rawCustomerProfileObject && 'custom' in rawCustomerProfileObject && rawCustomerProfileObject.custom && 'isLoyaltyMember' in rawCustomerProfileObject.custom && rawCustomerProfileObject.custom.isLoyaltyMember;
    var loyaltyPoints = rawCustomerProfileObject && 'custom' in rawCustomerProfileObject && rawCustomerProfileObject.custom && 'loyaltyTotalPoints' in rawCustomerProfileObject.custom ? rawCustomerProfileObject.custom.loyaltyTotalPoints : 0;
    var loyaltyPointsActivity = rawCustomerProfileObject && rawCustomerProfileObject.custom && rawCustomerProfileObject.custom.loyaltyPointsActivity;
    var loyaltyTier = rawCustomerProfileObject && rawCustomerProfileObject.custom && rawCustomerProfileObject.custom.loyaltyTier;
    var loyaltyJoiningDate = rawCustomerProfileObject && rawCustomerProfileObject.custom && rawCustomerProfileObject.custom.loyaltyJoiningDate;
    var loyaltyRefreshDate = rawCustomerProfileObject && rawCustomerProfileObject.custom && rawCustomerProfileObject.custom.loyaltyRefreshDate;
    var loyaltyTotalOrderAmount = rawCustomerProfileObject && rawCustomerProfileObject.custom && rawCustomerProfileObject.custom.loyaltyTotalOrderAmount ? rawCustomerProfileObject.custom.loyaltyTotalOrderAmount : 0;
    var loyaltyTierOrderAmount = rawCustomerProfileObject && rawCustomerProfileObject.custom && rawCustomerProfileObject.custom.loyaltyTierOrderAmount ? rawCustomerProfileObject.custom.loyaltyTierOrderAmount : 0;
    var loyaltyPendingPoints = rawCustomerProfileObject && rawCustomerProfileObject.custom && rawCustomerProfileObject.custom.loyaltyPendingPoints ? rawCustomerProfileObject.custom.loyaltyPendingPoints : 0;
    var loyaltyTotalPoints = rawCustomerProfileObject && rawCustomerProfileObject.custom && rawCustomerProfileObject.custom.loyaltyTotalPoints ? rawCustomerProfileObject.custom.loyaltyTotalPoints : 0;
    if (profile) {
        result = {
            firstName: profile.firstName,
            lastName: profile.lastName,
            email: profile.email,
            phone: Object.prototype.hasOwnProperty.call(profile, 'phone') ? profile.phone : profile.phoneHome,
            password: '********',
            isLoyaltyMember: isLoyaltyMember,
            loyaltyPoints: loyaltyPoints.toString(),
            customerNo: profile.customerNo,
            loyaltyPointsActivity: JSON.parse(loyaltyPointsActivity),
            loyaltyTier: loyaltyTier,
            loyaltyJoiningDate: loyaltyJoiningDate,
            loyaltyRefreshDate: loyaltyRefreshDate,
            loyaltyTotalOrderAmount: loyaltyTotalOrderAmount.toString(),
            loyaltyTierOrderAmount: loyaltyTierOrderAmount.toString(),
            loyaltyPendingPoints: loyaltyPendingPoints.toString(),
            loyaltyTotalPoints: loyaltyTotalPoints.toString()
        };
    } else {
        result = null;
    }
    return result;
}

/**
 * Account class that represents the current customer's profile dashboard
 * @param {Object} currentCustomer - Current customer
 * @param {Object} addressModel - The current customer's preferred address
 * @param {Object} orderModel - The current customer's order history
 * @constructor
 */
function account(currentCustomer, addressModel, orderModel) {
    base.call(this, currentCustomer, addressModel, orderModel);
    this.profile = getProfile(currentCustomer.profile, currentCustomer.raw.profile);
}

module.exports = account;

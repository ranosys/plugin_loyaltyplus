'use strict';

var base = module.superModule;
var HashMap = require('dw/util/HashMap');
var Template = require('dw/util/Template');
var Transaction = require('dw/system/Transaction');
var formatMoney = require('dw/util/StringUtils').formatMoney;
var collections = require('*/cartridge/scripts/util/collections');
var Site = require('dw/system/Site');

/**
 * Accepts a total object and formats the value
 * @param {dw.value.Money} total - Total price of the cart
 * @returns {string} the formatted money value
 */
function getTotals(total) {
    return !total.available ? '-' : formatMoney(total);
}

/**
 *
 * @param {Object} lineItemContainer order or basket object
 * @returns {string} formatted rewards amount
 */
function rewardsTotal(lineItemContainer) {
    var BasketMgr = require('dw/order/BasketMgr');
    var Order = require('dw/order/Order');
    var Basket = require('dw/order/Basket');
    var Money = require('dw/value/Money');
    var AmountDiscount = require('dw/campaign/AmountDiscount');
    var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
    var pointsToDollarsConversionRate = Site.getCurrent().getCustomPreferenceValue('PointsToDollarsConversionRate');
    var redemptionLimitPercentage = Site.getCurrent().getCustomPreferenceValue('redemptionLimitPercentage');
    var loyaltyRedeemptionPromo = Site.getCurrent().getCustomPreferenceValue('loyaltyRedeemptionPromo');
    var subtotal = 0;
    var redemptionValue;
    var profile = lineItemContainer.customer.profile;
    var availablePoints = 'custom' in profile && profile.custom && profile.custom.loyaltyTotalPoints ? profile.custom.loyaltyTotalPoints : 0;
    var productLineItems = lineItemContainer.getAllProductLineItems();
    for (var i = 0; i < productLineItems.length; i++) {
        var productLineItem = productLineItems[i];
        subtotal += productLineItem.adjustedPrice.value;
    }
    redemptionValue = (subtotal * (redemptionLimitPercentage / 100));
    if (redemptionValue > availablePoints / pointsToDollarsConversionRate) {
        redemptionValue = availablePoints / pointsToDollarsConversionRate;
    }
    var currentBasket = BasketMgr.getCurrentBasket();
    if (currentBasket && lineItemContainer instanceof Basket) {
        var discount = new AmountDiscount(redemptionValue);
        if (currentBasket.priceAdjustments.length) {
            for (var j = 0; j < currentBasket.priceAdjustments.length; j++) {
                var priceAdjustment = currentBasket.priceAdjustments[j];
                if (priceAdjustment.promotionID === loyaltyRedeemptionPromo) {
                    currentBasket.removePriceAdjustment(currentBasket.getPriceAdjustmentByPromotionID(loyaltyRedeemptionPromo));
                }
            }
        }
        Transaction.wrap(function () {
            currentBasket.createPriceAdjustment(loyaltyRedeemptionPromo, discount);
            currentBasket.custom.loyaltyPointsRedeemed = Math.round(redemptionValue * pointsToDollarsConversionRate);
            currentBasket.custom.loyaltyRedemptionAmount = redemptionValue.toFixed(2);
            basketCalculationHelpers.calculateTotals(currentBasket);
            currentBasket.custom.isLoyaltyRedemption = true;
        });
    } else if (lineItemContainer instanceof Order) {
        var OrderMgr = require('dw/order/OrderMgr');
        var order = OrderMgr.getOrder(lineItemContainer.currentOrderNo);
        redemptionValue = order.custom.loyaltyRedemptionAmount;
    }
    var redeemableRewards = new Money(redemptionValue, lineItemContainer.merchandizeTotalNetPrice.currencyCode);
    return redeemableRewards;
}

/**
 * Adds discounts to a discounts object
 * @param {dw.util.Collection} collection - a collection of price adjustments
 * @param {Object} discounts - an object of price adjustments
 * @returns {Object} an object of price adjustments
 */
function createDiscountObject(collection, discounts) {
    var result = discounts;
    var loyaltyRedeemptionPromo = Site.getCurrent().getCustomPreferenceValue('loyaltyRedeemptionPromo');
    collections.forEach(collection, function (item) {
        if (!item.basedOnCoupon && item.promotionID !== loyaltyRedeemptionPromo) {
            result[item.UUID] = {
                UUID: item.UUID,
                lineItemText: item.lineItemText,
                price: formatMoney(item.price),
                type: 'promotion',
                callOutMsg: (typeof item.promotion !== 'undefined' && item.promotion !== null) ? item.promotion.calloutMsg : ''
            };
        }
    });

    return result;
}

/**
 * creates an array of discounts.
 * @param {dw.order.LineItemCtnr} lineItemContainer - the current line item container
 * @returns {Array} an array of objects containing promotion and coupon information
 */
function getDiscounts(lineItemContainer) {
    var discounts = {};

    collections.forEach(lineItemContainer.couponLineItems, function (couponLineItem) {
        var priceAdjustments = collections.map(couponLineItem.priceAdjustments, function (priceAdjustment) {
            return { callOutMsg: (typeof priceAdjustment.promotion !== 'undefined' && priceAdjustment.promotion !== null) ? priceAdjustment.promotion.calloutMsg : '' };
        });
        discounts[couponLineItem.UUID] = {
            type: 'coupon',
            UUID: couponLineItem.UUID,
            couponCode: couponLineItem.couponCode,
            applied: couponLineItem.applied,
            valid: couponLineItem.valid,
            relationship: priceAdjustments
        };
    });

    discounts = createDiscountObject(lineItemContainer.priceAdjustments, discounts);
    discounts = createDiscountObject(lineItemContainer.allShippingPriceAdjustments, discounts);

    return Object.keys(discounts).map(function (key) {
        return discounts[key];
    });
}

/**
 * Gets the order discount amount by subtracting the basket's total including the discount from
 *      the basket's total excluding the order discount.
 * @param {dw.order.LineItemCtnr} lineItemContainer - Current users's basket
 * @returns {Object} an object that contains the value and formatted value of the order discount
 */
function getOrderLevelDiscountTotal(lineItemContainer) {
    var totalExcludingOrderDiscount = lineItemContainer.getAdjustedMerchandizeTotalPrice(false);
    var totalIncludingOrderDiscount = lineItemContainer.getAdjustedMerchandizeTotalPrice(true);
    var orderDiscount = totalExcludingOrderDiscount.subtract(totalIncludingOrderDiscount);
    if ('custom' in lineItemContainer && 'isLoyaltyRedemption' in lineItemContainer.custom && lineItemContainer.custom.isLoyaltyRedemption) {
        var rewardsAmount = rewardsTotal(lineItemContainer);
        orderDiscount = orderDiscount.subtract(rewardsAmount);
    }

    return {
        value: orderDiscount.value,
        formatted: formatMoney(orderDiscount)
    };
}

/**
 * create the discount results html
 * @param {Array} discounts - an array of objects that contains coupon and priceAdjustment
 * information
 * @returns {string} The rendered HTML
 */
function getDiscountsHtml(discounts) {
    var context = new HashMap();
    var object = { totals: { discounts: discounts } };

    Object.keys(object).forEach(function (key) {
        context.put(key, object[key]);
    });

    var template = new Template('cart/cartCouponDisplay');
    return template.render(context).text;
}

/**
 * @constructor
 * @classdesc totals class that represents the order totals of the current line item container
 *
 * @param {dw.order.lineItemContainer} lineItemContainer - The current user's line item container
 */
function totals(lineItemContainer) {
    base.call(this, lineItemContainer);
    if (lineItemContainer) {
        if ('custom' in lineItemContainer && 'isLoyaltyRedemption' in lineItemContainer.custom && lineItemContainer.custom.isLoyaltyRedemption) {
            var rewardsAmount = rewardsTotal(lineItemContainer);
            this.rewardsTotal = getTotals(rewardsAmount);
        } else {
            this.rewardsTotal = '-';
        }
        this.orderLevelDiscountTotal = getOrderLevelDiscountTotal(lineItemContainer);
        this.discounts = getDiscounts(lineItemContainer);
        this.discountsHtml = getDiscountsHtml(this.discounts);
    }
}

module.exports = totals;

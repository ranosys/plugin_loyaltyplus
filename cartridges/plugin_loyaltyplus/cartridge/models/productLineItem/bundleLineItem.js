'use strict';

var base = module.superModule;
var system = require('dw/system');
var site = system.Site.getCurrent();
var productLineItemDecorators = require('*/cartridge/models/productLineItem/decorators/index');

module.exports = function productLineItem(product, apiProduct, options, factory) {
    base.call(this, product, apiProduct, options, factory);
    var enableLoyaltyPlus = site.getCustomPreferenceValue('enableLoyaltyPlus');
    if (enableLoyaltyPlus) {
        productLineItemDecorators.loyaltyPoints(product);
    }
    return product;
};

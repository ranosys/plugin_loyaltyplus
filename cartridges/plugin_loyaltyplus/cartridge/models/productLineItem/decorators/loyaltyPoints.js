'use strict';

var system = require('dw/system');
var site = system.Site.getCurrent();

/**
  * @param {Object} object - product data
  * @returns {number} - returns points to be displayed on PLP and PDP, based on user type
*/
function getPoints(object) {
    // eslint-disable-next-line no-undef
    var customer = request.session.customer;
    var loyaltyTier = JSON.parse(site.getCustomPreferenceValue('loyaltyTier'));
    var loyaltyHelper = require('*/cartridge/scripts/helpers/loyaltyHelpers');
    var pointsPerDollar;
    var points = 0;
    if (customer && customer.authenticated && customer.profile.custom.loyaltyTier) {
        var currentTier = customer.profile.custom.loyaltyTier;
        var currentTierDetails = loyaltyHelper.getTierDetails(currentTier, loyaltyTier);
        pointsPerDollar = currentTierDetails.pointsPerDollar;
    } else {
        var tierArray = [];
        // created array to store element having tier and its pointsPerDollar value information
        Object.keys(loyaltyTier).forEach(function (key) {
            tierArray.push({ tierType: key, value: loyaltyTier[key].min, pointsPerDollar: loyaltyTier[key].pointsPerDollar });
        });
        // created array to store element having tier and its pointsPerDollar value information

        // searching element from array having max value
        var loyaltyTierObject = tierArray.reduce(function (max, current) {
            return current.value > max.value ? current : max;
        });
        // searching element from array having max value
        pointsPerDollar = loyaltyTierObject.pointsPerDollar;
    }
    if (object.priceTotal && object.priceTotal.value) {
        points = Math.round(pointsPerDollar * object.priceTotal.value).toString();
    }
    return points;
}

module.exports = function (object) {
    Object.defineProperty(object, 'loyaltyPoints', {
        enumerable: true,
        value: getPoints(object)
    });
};

'use strict';

var base = module.superModule;

base.loyaltyPoints = require('*/cartridge/models/productLineItem/decorators/loyaltyPoints');

module.exports = base;

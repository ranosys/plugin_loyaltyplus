'use strict';

var base = module.superModule;
var system = require('dw/system');
var site = system.Site.getCurrent();
var decorators = require('*/cartridge/models/productLineItem/decorators/index');

module.exports = function productLineItem(product, apiProduct, options) {
    base.call(this, product, apiProduct, options);
    var enableLoyaltyPlus = site.getCustomPreferenceValue('enableLoyaltyPlus');
    if (enableLoyaltyPlus) {
        decorators.loyaltyPoints(product);
    }
    return product;
};

'use strict';

var base = module.superModule;

var system = require('dw/system');
var site = system.Site.getCurrent();
var loyaltyHelper = require('*/cartridge/scripts/helpers/loyaltyHelpers');

/**
 *
 * @param {Object} basket current basket
 * @returns {string} points in amount to be reddemed
 */
function getRedeemablePoints(basket) {
    var profile = basket.customer.profile;
    var availablePoints = profile && 'custom' in profile && profile.custom && profile.custom.loyaltyTotalPoints ? profile.custom.loyaltyTotalPoints : 0;
    var subtotal = 0;
    var productLineItems = basket.getAllProductLineItems();
    for (var i = 0; i < productLineItems.length; i++) {
        var productLineItem = productLineItems[i];
        subtotal += productLineItem.adjustedPrice.value;
    }
    var pointsToDollarsConversionRate = site.getCustomPreferenceValue('PointsToDollarsConversionRate');
    var redemptionLimitPercentage = site.getCustomPreferenceValue('redemptionLimitPercentage');
    var redemptionValue = (subtotal * (redemptionLimitPercentage / 100));
    if (redemptionValue > availablePoints / pointsToDollarsConversionRate) {
        redemptionValue = availablePoints / pointsToDollarsConversionRate;
    }
    var redeemablePoints = redemptionValue * pointsToDollarsConversionRate;
    return Math.round(redeemablePoints).toString();
}

/**
 * @constructor
 * @classdesc CartModel class that represents the current basket
 *
 * @param {dw.order.Basket} basket - Current users's basket
 */
function CartModel(basket) {
    base.call(this, basket);
    if (basket) {
        // eslint-disable-next-line no-undef
        var customer = request.session.customer;
        if (loyaltyHelper.isOrderFromValidMember(customer)) {
            this.redeemablePoints = getRedeemablePoints(basket);
            this.isLoyaltyRedemption = 'custom' in basket && 'isLoyaltyRedemption' in basket.custom && basket.custom.isLoyaltyRedemption;
        }
    }
}

module.exports = CartModel;

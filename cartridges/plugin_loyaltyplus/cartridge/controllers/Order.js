'use strict';

var server = require('server');
server.extend(module.superModule);
var Transaction = require('dw/system/Transaction');
var OrderMgr = require('dw/order/OrderMgr');
var CustomerMgr = require('dw/customer/CustomerMgr');
var system = require('dw/system');
var site = system.Site.getCurrent();

/**
  * Order-Confirm : This endpoint is invoked when the shopper's Order is Placed and Confirmed
  * @name Base/Order-Confirm
  * @function
  * @memberof Order
  */
server.append('Confirm', function (req, res, next) {
    var enableLoyaltyPlus = site.getCustomPreferenceValue('enableLoyaltyPlus');
    var loyaltyHelper = require('*/cartridge/scripts/helpers/loyaltyHelpers');
    if (enableLoyaltyPlus) {
        var customer = req.currentCustomer.raw;
        // update loyalty pending points
        if (loyaltyHelper.isOrderFromValidMember(customer)) {
            var loyaltyTier = JSON.parse(site.getCustomPreferenceValue('loyaltyTier'));
            var currentTier = customer.profile.custom.loyaltyTier;
            var currentTierDetails = loyaltyHelper.getTierDetails(currentTier, loyaltyTier);
            var pointsPerDollar = currentTierDetails.pointsPerDollar;
            var viewData = res.getViewData();
            var order = OrderMgr.getOrder(viewData.order.orderNumber);
            var productLineItems = order.getAllProductLineItems();
            var totalNetPrice = 0;
            for (var i = 0; i < productLineItems.length; i++) {
                var productLineItem = productLineItems[i];
                totalNetPrice += productLineItem.adjustedPrice.value;
            }
            var points = Math.round(pointsPerDollar * totalNetPrice);

            var customerObj = CustomerMgr.getCustomerByCustomerNumber(
                customer.profile.customerNo
            );
            var customerPendingLoyaltyPoint = customerObj.profile.custom.loyaltyPendingPoints;
            var updatedPendingLoyaltyPoint = customerPendingLoyaltyPoint + points;
            Transaction.wrap(function () {
                customerObj.profile.custom.loyaltyPendingPoints = Math.round(updatedPendingLoyaltyPoint);
                customerObj.profile.custom.loyaltyTotalPoints -= order.custom && order.custom.loyaltyPointsRedeemed ? Math.round(order.custom.loyaltyPointsRedeemed) : 0;
                order.custom.loyaltyOrderPoints = points;
                order.custom.isLoyaltyPointUpdated = false;
                order.custom.isLoyaltyOrder = true;
            });
        }
        // update loyalty pending points
    }
    next();
});

module.exports = server.exports();

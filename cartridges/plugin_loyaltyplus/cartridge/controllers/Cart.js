'use strict';

var server = require('server');
server.extend(module.superModule);
var system = require('dw/system');
var site = system.Site.getCurrent();

/**
  * Cart-RemoveProductLineItem : This endpoint is invoked when shoppers land on PDP
  * @name Base/Cart-RemoveProductLineItem
  * @function
  * @memberof Cart
  */
server.append('RemoveProductLineItem', function (req, res, next) {
    var enableLoyaltyPlus = site.getCustomPreferenceValue('enableLoyaltyPlus');
    if (enableLoyaltyPlus) {
        var loyaltyRedeemptionPromo = site.getCustomPreferenceValue('loyaltyRedeemptionPromo');
        var BasketMgr = require('dw/order/BasketMgr');
        var Transaction = require('dw/system/Transaction');
        var currentBasket = BasketMgr.getCurrentBasket();
        var allProductLineItems = currentBasket.getAllProductLineItems();
        if (allProductLineItems && allProductLineItems.length <= 0) {
            Transaction.wrap(function () {
                if (currentBasket.priceAdjustments.length) {
                    for (var i = 0; i < currentBasket.priceAdjustments.length; i++) {
                        var priceAdjustment = currentBasket.priceAdjustments[i];
                        if (priceAdjustment.promotionID === loyaltyRedeemptionPromo) {
                            currentBasket.removePriceAdjustment(currentBasket.getPriceAdjustmentByPromotionID(loyaltyRedeemptionPromo));
                        }
                    }
                }
                currentBasket.custom.loyaltyPointsRedeemed = 0;
                currentBasket.custom.loyaltyRedemptionAmount = 0;
                currentBasket.custom.isLoyaltyRedemption = false;
            });
        }
    }
    next();
});

module.exports = server.exports();

'use strict';

var server = require('server');
var Resource = require('dw/web/Resource');
var URLUtils = require('dw/web/URLUtils');
var OrderMgr = require('dw/order/OrderMgr');
var Transaction = require('dw/system/Transaction');
var Calendar = require('dw/util/Calendar');
var StringUtils = require('dw/util/StringUtils');
var userLoggedIn = require('*/cartridge/scripts/middleware/userLoggedIn');
var CustomerMgr = require('dw/customer/CustomerMgr');
var Currency = require('dw/util/Currency');
var system = require('dw/system');
var site = system.Site.getCurrent();

/**
 * Loyalty-Enableloyalty : The Loyalty-Enableloyalty endpoint is used to save birthday and assign default loyalty tier to the customers
 * @name Loyalty-Enableloyalty
 * @function
 * @memberof Loyalty
 * @param {middleware} - server.middleware.https
 * @param {category} - sensitive
 * @param {returns} - json
 * @param {serverfunction} - post
 */
server.post(
    'Enableloyalty',
    server.middleware.https,
    function (req, res, next) {
        var loyaltyTier = JSON.parse(site.getCustomPreferenceValue('loyaltyTier'));
        var loyaltyPointsEventList = JSON.parse(site.getCustomPreferenceValue('loyaltyPointsEventList'));
        var tierResetPeriod = site.getCustomPreferenceValue('tierResetPeriod');
        var requestData = req.httpParameterMap;
        var email = requestData.email.value;
        var customer = CustomerMgr.getCustomerByLogin(email);
        var profile = CustomerMgr.getProfile(customer.profile.customerNo);
        var tierArray = [];

        // created array to store element having tier and its min value information
        Object.keys(loyaltyTier).forEach(function (key) {
            tierArray.push({ tierType: key, value: loyaltyTier[key].min });
        });
        // created array to store element having tier and its min value information

        // searching element from array having min value to assign by default while registration
        var loyaltyTierObject = tierArray.reduce(function (min, current) {
            return current.value < min.value ? current : min;
        });
        // searching element from array having min value to assign by default while registration

        // loyalty activity
        var pointHistory = [];
        var type = Resource.msg('loyalty.enroll.text', 'loyalty', null);
        pointHistory.push({
            date: new Date(), type: type, points: loyaltyPointsEventList.enroll, isEarned: true
        });
        // loyalty activity
        var currentDate = new Date();
        var refreshDate = new Date(currentDate.setDate(currentDate.getDate() + tierResetPeriod));
        Transaction.wrap(function () {
            profile.custom.isLoyaltyMember = requestData.enableloyalty.booleanValue;
            profile.birthday = new Date(requestData.birthday.value);
            profile.custom.loyaltyTier = loyaltyTierObject.tierType;
            profile.custom.loyaltyPointsActivity = JSON.stringify(pointHistory);
            profile.custom.loyaltyJoiningDate = new Date();
            profile.custom.loyaltyRefreshDate = refreshDate;
            profile.custom.loyaltyTotalOrderAmount = 0;
            profile.custom.loyaltyTierOrderAmount = 0;
            profile.custom.loyaltyPendingPoints = 0;
            profile.custom.loyaltyTotalPoints = loyaltyPointsEventList.enroll;
        });

        res.json({ success: true });
        next();
    }
);

/**
 * Loyalty-Dashboard : The Loyalty-Dashboard endpoint renders the page that allows a shopper to see his/her loyalty dashboard
 * @name Loyalty-Dashboard
 * @function
 * @memberof Loyalty
 * @param {middleware} - server.middleware.https
 * @param {middleware} - userLoggedIn.validateLoggedIn
 * @param {category} - sensitive
 * @param {renders} - isml
 * @param {serverfunction} - get
 */
server.get(
    'Dashboard',
    server.middleware.https,
    userLoggedIn.validateLoggedIn,
    function (req, res, next) {
        var accountHelpers = require('*/cartridge/scripts/account/accountHelpers');
        var loyaltyHelper = require('*/cartridge/scripts/helpers/loyaltyHelpers');
        var loyaltyTier = JSON.parse(site.getCustomPreferenceValue('loyaltyTier'));
        var accountModel = accountHelpers.getAccountModel(req);

        // formatted joining and refresh date
        var joiningDatecalendar = new Calendar(accountModel.profile.loyaltyJoiningDate);
        var formattedJoiningDate = StringUtils.formatCalendar(joiningDatecalendar, 'dd MMMM yyyy');
        accountModel.profile.loyaltyJoiningDate = formattedJoiningDate;

        var refreshDatecalendar = new Calendar(accountModel.profile.loyaltyRefreshDate);
        var formattedRefreshDate = StringUtils.formatCalendar(refreshDatecalendar, 'dd MMMM yyyy');
        accountModel.profile.loyaltyRefreshDate = formattedRefreshDate;
        // formatted joining and refresh date
        var currentTierDetails = loyaltyHelper.getTierDetails(accountModel.profile.loyaltyTier, loyaltyTier);
        res.render('loyalty/loyaltyDashboard', {
            account: accountModel,
            angle: currentTierDetails.speedoMeterAngle,
            breadcrumbs: [
                {
                    htmlValue: Resource.msg('global.home', 'common', null),
                    url: URLUtils.home().toString()
                },
                {
                    htmlValue: Resource.msg('page.title.myaccount', 'account', null),
                    url: URLUtils.url('Account-Show').toString()
                }
            ]
        });
        next();
    }
);

/**
 * Loyalty-PointHistory : The Loyalty-PointHistory endpoint renders the page that allows a shopper to see his/her loyalty activity
 * @name Loyalty-PointHistory
 * @function
 * @memberof Loyalty
 * @param {middleware} - server.middleware.https
 * @param {middleware} - userLoggedIn.validateLoggedIn
 * @param {category} - sensitive
 * @param {renders} - isml
 * @param {serverfunction} - get
 */
server.get(
    'ActivityHistory',
    server.middleware.https,
    userLoggedIn.validateLoggedIn,
    function (req, res, next) {
        var accountHelpers = require('*/cartridge/scripts/account/accountHelpers');

        var accountModel = accountHelpers.getAccountModel(req);
        var pointHistory = [];
        var loyaltyOrderPoints;
        var loyaltyPointsRedeemed;
        var orderStatus = {};
        orderStatus.ORDER_STATUS_COMPLETED = 5;
        orderStatus.ORDER_STATUS_NEW = 3;
        orderStatus.ORDER_STATUS_OPEN = 4;
        var queryString = 'customerNo = {0} AND custom.isLoyaltyOrder = {1} AND (status = {2} OR status = {3} OR status = {4})';
        var ordersIterator = OrderMgr.queryOrders(queryString, 'orderNo ASC', accountModel.profile.customerNo, true, orderStatus.ORDER_STATUS_COMPLETED, orderStatus.ORDER_STATUS_NEW, orderStatus.ORDER_STATUS_OPEN);
        while (ordersIterator.hasNext()) {
            var order = ordersIterator.next();
            var type = Resource.msgf('loyalty.order.purchase.text', 'loyalty', null, order.currentOrderNo);
            var calendar = new Calendar(order.creationDate);
            var formattedDate = StringUtils.formatCalendar(calendar, 'dd MMMM yyyy');
            loyaltyOrderPoints = order.custom.loyaltyOrderPoints.toString();
            if (order.custom.isLoyaltyPointUpdated) {
                pointHistory.push({
                    date: formattedDate, type: type, points: loyaltyOrderPoints, isEarned: true, isPending: false
                });
                if (order.custom.loyaltyPointsRedeemed) {
                    loyaltyPointsRedeemed = order.custom.loyaltyPointsRedeemed.toString();
                    pointHistory.push({
                        date: formattedDate, type: type, points: loyaltyPointsRedeemed, isEarned: false, isPending: false
                    });
                }
            } else {
                pointHistory.push({
                    date: formattedDate, type: type, points: loyaltyOrderPoints, isEarned: true, isPending: true
                });
            }
        }
        var updateActivityArray = [];
        var loyaltyPointsActivity = accountModel.profile.loyaltyPointsActivity;
        if (loyaltyPointsActivity) {
            for (var i = 0; i < loyaltyPointsActivity.length; i += 1) {
                var activity = loyaltyPointsActivity[i];
                var activityCalendar = new Calendar(new Date(activity.date));
                var activityFormattedDate = StringUtils.formatCalendar(activityCalendar, 'dd MMMM yyyy');

                activity.date = activityFormattedDate;
                updateActivityArray.push(activity);
            }
        }

        // merging two arrays
        var mergedPointHistory = updateActivityArray.slice();
        pointHistory.forEach(function (item) {
            mergedPointHistory.push(item);
        });
        // merging two arrays

        // sorting merged array based on date
        mergedPointHistory.sort(function (a, b) {
            return a.date - b.date;
        });
        // sorting merged array based on date

        res.render('loyalty/loyaltyPointHistory', {
            pointHistory: mergedPointHistory.reverse(),
            breadcrumbs: [
                {
                    htmlValue: Resource.msg('global.home', 'common', null),
                    url: URLUtils.home().toString()
                },
                {
                    htmlValue: Resource.msg('page.title.myaccount', 'account', null),
                    url: URLUtils.url('Account-Show').toString()
                }
            ]
        });
        next();
    }
);

/**
 * Loyalty-RedeemRewards : The Loyalty-RedeemRewards endpoint is used to redeem loyalty points
 * @name Loyalty-RedeemRewards
 * @function
 * @memberof Loyalty
 * @param {middleware} - server.middleware.https
 * @param {middleware} - userLoggedIn.validateLoggedIn
 * @param {category} - sensitive
 * @param {returns} - json
 * @param {serverfunction} - post
 */
server.post(
    'RedeemRewards',
    server.middleware.https,
    userLoggedIn.validateLoggedIn,
    function (req, res, next) {
        var BasketMgr = require('dw/order/BasketMgr');
        var CartModel = require('*/cartridge/models/cart');
        var AmountDiscount = require('dw/campaign/AmountDiscount');
        var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
        var currentBasket = BasketMgr.getCurrentBasket();
        var redeemRewards = false;
        var requestData = req.httpParameterMap;
        redeemRewards = 'redeemRewards' in requestData && requestData.redeemRewards.booleanValue;
        if (currentBasket) {
            var profile = currentBasket.customer.profile;
            var availablePoints = 'custom' in profile && profile.custom && profile.custom.loyaltyTotalPoints ? profile.custom.loyaltyTotalPoints : 0;
            var pointsToDollarsConversionRate = site.getCustomPreferenceValue('PointsToDollarsConversionRate');
            var redemptionLimitPercentage = site.getCustomPreferenceValue('redemptionLimitPercentage');
            var loyaltyRedeemptionPromo = site.getCustomPreferenceValue('loyaltyRedeemptionPromo');
            var subtotal = 0;
            var productLineItems = currentBasket.getAllProductLineItems();
            for (var i = 0; i < productLineItems.length; i++) {
                var productLineItem = productLineItems[i];
                subtotal += productLineItem.adjustedPrice.value;
            }
            var redemptionValue = subtotal * (redemptionLimitPercentage / 100);
            if (redemptionValue > availablePoints / pointsToDollarsConversionRate) {
                redemptionValue = availablePoints / pointsToDollarsConversionRate;
            }
            var discount = new AmountDiscount(redemptionValue);
            Transaction.wrap(function () {
                if (redeemRewards) {
                    currentBasket.createPriceAdjustment(loyaltyRedeemptionPromo, discount);
                    currentBasket.custom.loyaltyPointsRedeemed = Math.round(redemptionValue * pointsToDollarsConversionRate);
                    currentBasket.custom.loyaltyRedemptionAmount = redemptionValue.toFixed(2);
                } else {
                    currentBasket.removePriceAdjustment(currentBasket.getPriceAdjustmentByPromotionID(loyaltyRedeemptionPromo));
                    currentBasket.custom.loyaltyPointsRedeemed = 0;
                    currentBasket.custom.loyaltyRedemptionAmount = 0;
                }
                basketCalculationHelpers.calculateTotals(currentBasket);
                currentBasket.custom.isLoyaltyRedemption = redeemRewards;
            });
        }
        var basketModel = new CartModel(currentBasket);
        if (requestData.page) {
            basketModel.page = requestData.page.value;
        }
        res.json(basketModel);
        next();
    }
);

/**
 * Loyalty-EarnPoints : The Loyalty-EarnPoints endpoint renders the information on how to earn points
 * @name Loyalty-EarnPoints
 * @function
 * @memberof Loyalty
 * @param {middleware} - server.middleware.https
 * @param {middleware} - userLoggedIn.validateLoggedIn
 * @param {category} - sensitive
 * @param {renders} - isml
 * @param {serverfunction} - get
 */
server.get(
    'EarnPoints',
    server.middleware.https,
    userLoggedIn.validateLoggedIn,
    function (req, res, next) {
        var loyaltyTier = JSON.parse(site.getCustomPreferenceValue('loyaltyTier'));
        var loyaltyPointsEventList = JSON.parse(site.getCustomPreferenceValue('loyaltyPointsEventList'));
        var membershipLevelsArray = Object.keys(loyaltyTier).map(function(key) {
            return loyaltyTier[key];
        });
        var currency = Currency.getCurrency(req.session.currency.currencyCode);
        for (var i = 0; i < membershipLevelsArray.length; i++) {
            membershipLevelsArray[i].min = currency.getSymbol() + membershipLevelsArray[i].min;
            if (membershipLevelsArray[i].max) {
                membershipLevelsArray[i].max = currency.getSymbol() + membershipLevelsArray[i].max;
            }
        }
        res.render('loyalty/loyaltyEarnPoints', {
            loyaltyTier: membershipLevelsArray,
            loyaltyPointsEventList: loyaltyPointsEventList,
            breadcrumbs: [
                {
                    htmlValue: Resource.msg('global.home', 'common', null),
                    url: URLUtils.home().toString()
                },
                {
                    htmlValue: Resource.msg('page.title.myaccount', 'account', null),
                    url: URLUtils.url('Account-Show').toString()
                }
            ]
        });
        next();
    }
);

module.exports = server.exports();

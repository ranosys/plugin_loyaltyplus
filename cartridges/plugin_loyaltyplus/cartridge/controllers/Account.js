'use strict';

var server = require('server');
server.extend(module.superModule);


/**
  * Account-SubmitRegistration : This endpoint is invoked when the shopper's submits registration
  * @name Base/Account-SubmitRegistration
  * @function
  * @memberof Account
  */
server.prepend('SubmitRegistration', function (req, res, next) {
    var formErrors = require('*/cartridge/scripts/formErrors');
    var Resource = require('dw/web/Resource');
    var registrationForm = server.forms.getForm('profile');
    var isloyalty = registrationForm.customer.addtoloyalty.value;
    if (isloyalty) {
        var birthday = registrationForm.customer.dob.value;
        if (!birthday) {
            registrationForm.customer.dob.valid = false;
            registrationForm.customer.dob.error = Resource.msg('error.message.required', 'login', null);
            registrationForm.valid = false;
        } else {
            var birthdate = new Date(registrationForm.customer.dob.value);
            var currentDate = new Date();
            if (birthdate > currentDate) {
                registrationForm.customer.dob.valid = false;
                registrationForm.customer.dob.error = Resource.msg('loyalty.dob.future.error.message', 'loyalty', null);
                registrationForm.valid = false;
            }
        }
        if (!registrationForm.valid) {
            res.json({
                fields: formErrors.getFormErrors(registrationForm)
            });
            this.emit('route:Complete', req, res);
            return;
        }
    }
    next();
});

/**
  * Account-SubmitRegistration : This endpoint is invoked when the shopper's submits registration
  * @name Base/Account-SubmitRegistration
  * @function
  * @memberof Account
  */
server.append('SubmitRegistration', function (req, res, next) {
    var URLUtils = require('dw/web/URLUtils');
    var viewData = res.getViewData();
    viewData.isloyalty = 'form' in viewData && viewData.form && 'base' in viewData.form && viewData.form.base && 'customer' in viewData.form.base && viewData.form.base.customer && 'addtoloyalty' in viewData.form.base.customer && viewData.form.base.customer.addtoloyalty && viewData.form.base.customer.addtoloyalty.checked ? viewData.form.base.customer.addtoloyalty.checked : false;
    viewData.birthday = 'form' in viewData && viewData.form && 'base' in viewData.form && viewData.form.base && 'customer' in viewData.form.base && viewData.form.base.customer && 'dob' in viewData.form.base.customer && viewData.form.base.customer.dob ? viewData.form.base.customer.dob.value : null;
    viewData.loyaltyURL = URLUtils.url('Loyalty-Enableloyalty').toString();
    next();
});

module.exports = server.exports();

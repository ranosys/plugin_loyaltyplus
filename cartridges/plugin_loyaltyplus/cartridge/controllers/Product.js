'use strict';

var server = require('server');
server.extend(module.superModule);
var system = require('dw/system');
var site = system.Site.getCurrent();

/**
  * Product-Show : This endpoint is invoked when shoppers land on PDP
  * @name Base/Product-Show
  * @function
  * @memberof Product
  */
server.append('Show', function (req, res, next) {
    var enableLoyaltyPlus = site.getCustomPreferenceValue('enableLoyaltyPlus');
    var loyaltyHelper = require('*/cartridge/scripts/helpers/loyaltyHelpers');
    if (enableLoyaltyPlus) {
        var customer = req.currentCustomer.raw;
        var viewData = res.getViewData();
        viewData.customer = customer;
        viewData.pointsPerDollar = loyaltyHelper.getPoints(req);
        res.setViewData(viewData);
    }
    next();
});

module.exports = server.exports();

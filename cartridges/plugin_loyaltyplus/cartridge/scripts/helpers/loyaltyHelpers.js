'use strict';

var Logger = require('dw/system/Logger').getLogger('LoyaltyPlusLog', 'LoyaltyPlus');
var system = require('dw/system');
var Calendar = require('dw/util/Calendar');
var StringUtils = require('dw/util/StringUtils');
var Resource = require('dw/web/Resource');
var site = system.Site.getCurrent();

/**
 * @param {Object} customer - customer object
 * @returns {boolean} - return true if the customer is loyalty member, otherwise false
 */
function isOrderFromValidMember(customer) {
    var profile = customer.getProfile();
    if (profile && profile.custom && profile.custom.isLoyaltyMember) {
        return true;
    }
    return false;
}

/**
 * @param {string} currentTier current tier of the customer
 * @param {Object} loyaltyTier details of all the tier
 * @returns {Object} an object that contains information about the current customer's tier
 */
function getTierDetails(currentTier, loyaltyTier) {
    var tierDetails = {};
    Object.keys(loyaltyTier).forEach(function (key) {
        if (key === currentTier) {
            tierDetails = loyaltyTier[key];
        }
    });
    return tierDetails;
}

/**
 * Sends email to customer notifying about tier change
 * @param {Object} profile - profile data
 */
function sendMail(profile) {
    var Mail = require('dw/net/Mail');
    var HashMap = require('dw/util/HashMap');
    var Template = require('dw/util/Template');
    var context = new HashMap();
    var fromEmailIds;
    try {
        // Retrieve the 'from' email IDs from site preferences
        fromEmailIds = site.current.getCustomPreferenceValue('loyaltyPlusFromEmail');
        // Set 'from' email IDs for the notification email
        if (!fromEmailIds) {
            fromEmailIds ='no-reply@salesforce.com';
        }
    } catch (exception) {
        Logger.error('Error binding loyaltyPlus_FromEmail : ' + exception);
        fromEmailIds ='no-reply@salesforce.com';
    }
    var emailSubject = Resource.msg('loyalty.update.email.subject', 'loyalty', null)
    context.firstName = profile.firstName;
    context.updatedTier = (profile.custom.loyaltyTier).toUpperCase();
    var calendar = new Calendar(new Date());
    var formattedDate = StringUtils.formatCalendar(calendar, 'dd MMMM yyyy');
    context.refreshDate = formattedDate;

    try {
        var notificationEmail = new Mail();
        notificationEmail.addTo(profile.email);
        notificationEmail.setFrom(fromEmailIds);
        notificationEmail.setSubject(emailSubject);
        var template = new Template('loyalty/loyaltyUpdateEmail');
        var content = template.render(context).text;
        notificationEmail.setContent(content, 'text/html', 'UTF-8');
        notificationEmail.send();
    } catch (exception) {
        Logger.error('Unable to send email:' + exception);
    }
}

/**
  * @param {Object} request - request data
  * @returns {number} - returns points to be displayed on PLP and PDP, based on user type
*/
function getPoints(request) {
    var customer = request.session.customer;
    var loyaltyTier = JSON.parse(site.getCustomPreferenceValue('loyaltyTier'));
    var loyaltyHelper = require('*/cartridge/scripts/helpers/loyaltyHelpers');
    var pointsPerDollar;
    if (customer && customer.authenticated && customer.profile.custom.loyaltyTier) {
        var currentTier = customer.profile.custom.loyaltyTier;
        var currentTierDetails = loyaltyHelper.getTierDetails(currentTier, loyaltyTier);
        pointsPerDollar = currentTierDetails.pointsPerDollar;
    } else {
        var tierArray = [];
        // created array to store element having tier and its pointsPerDollar value information
        Object.keys(loyaltyTier).forEach(function (key) {
            tierArray.push({ tierType: key, value: loyaltyTier[key].min, pointsPerDollar: loyaltyTier[key].pointsPerDollar });
        });
        // created array to store element having tier and its pointsPerDollar value information

        // searching element from array having max value
        var loyaltyTierObject = tierArray.reduce(function (max, current) {
            return current.value > max.value ? current : max;
        });
        // searching element from array having max value
        pointsPerDollar = loyaltyTierObject.pointsPerDollar;
    }
    return pointsPerDollar;
}

module.exports = {
    isOrderFromValidMember: isOrderFromValidMember,
    getTierDetails: getTierDetails,
    sendMail: sendMail,
    getPoints: getPoints
};

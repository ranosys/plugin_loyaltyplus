'use strict';

var Transaction = require('dw/system/Transaction');
var Logger = require('dw/system/Logger').getLogger('LoyaltyPlusLog', 'LoyaltyPlus');
var Status = require('dw/system/Status');
var SeekableIterator = require('dw/util/SeekableIterator');
var SystemObjectMgr = require('dw/object/SystemObjectMgr');
var system = require('dw/system');
var site = system.Site.getCurrent();

/**
 * @param {number} loyaltyTierOrderAmount customer's total spend amount till date
 * @param {Object} loyaltyTier details of all the tier
 * @returns {string} an string which contains updated loyalty tier information based on amount
 */
function getUpdatedLoyaltyTier(loyaltyTierOrderAmount, loyaltyTier) {
    var updatedLoyaltyTier = '';
    Object.keys(loyaltyTier).forEach(function (key) {
        if (!loyaltyTier[key].max) {
            if (loyaltyTierOrderAmount >= loyaltyTier[key].min) {
                updatedLoyaltyTier = key;
            }
        } else if (loyaltyTierOrderAmount >= loyaltyTier[key].min && loyaltyTierOrderAmount <= loyaltyTier[key].max) {
            updatedLoyaltyTier = key;
        }
    });
    return updatedLoyaltyTier;
}

/**
 * @param {Object} profile profile object
 * @param {string} tier updated loyalty tier
 * @returns {boolean} always return true
 */
function updateTier(profile, tier) {
    var profileObj = profile;
    var tierResetPeriod = site.getCustomPreferenceValue('tierResetPeriod');
    var currentDate = new Date();
    var refreshDate = new Date(currentDate.setDate(currentDate.getDate() + tierResetPeriod));
    Transaction.wrap(function () {
        profileObj.custom.loyaltyTier = tier;
        profileObj.custom.loyaltyRefreshDate = refreshDate;
        profileObj.custom.loyaltyTierOrderAmount = 0;
    });
    return true;
}

/**
 * @param {Object} jobParams - check to see if product feed is enabled.
 * @returns {Status} - The updated status.
 */
function execute(jobParams) {
    // Check the job step is enabled or not
    if (!jobParams.Enabled) {
        return new Status(Status.OK, 'OK', 'Step disabled, skip it...');
    }
    var profileIterator = SystemObjectMgr.querySystemObjects('Profile', 'custom.isLoyaltyMember = {0}', null, true);
    try {
        while (profileIterator.hasNext()) {
            var loyaltyHelper = require('*/cartridge/scripts/helpers/loyaltyHelpers');
            var loyaltyTier = JSON.parse(site.getCustomPreferenceValue('loyaltyTier'));
            var profile = profileIterator.next();
            var loyaltyRefreshDate = profile.custom.loyaltyRefreshDate;
            var loyaltyTierOrderAmount = profile.custom.loyaltyTierOrderAmount;
            if (new Date() > loyaltyRefreshDate) {
                var currentTier = profile.custom.loyaltyTier;
                var currentTierDetails = loyaltyHelper.getTierDetails(currentTier, loyaltyTier);
                var isTotalAmountSatisfied = false;
                if (!currentTierDetails.max) {
                    if (loyaltyTierOrderAmount >= currentTierDetails.min) {
                        isTotalAmountSatisfied = true;
                    } else {
                        isTotalAmountSatisfied = false;
                    }
                } else if (loyaltyTierOrderAmount >= currentTierDetails.min && loyaltyTierOrderAmount <= currentTierDetails.max) {
                    isTotalAmountSatisfied = true;
                } else {
                    isTotalAmountSatisfied = false;
                }
                if (!isTotalAmountSatisfied) {
                    var updatedLoyaltyTier = getUpdatedLoyaltyTier(loyaltyTierOrderAmount, loyaltyTier);
                    updateTier(profile, updatedLoyaltyTier);
                    loyaltyHelper.sendMail(profile);
                } else {
                    updateTier(profile, currentTier);
                }
            }
        }
        if (profileIterator instanceof SeekableIterator) {
            profileIterator.close();
        }
        return new Status(Status.OK, null, 'updateTierStatus job finished');
    } catch (error) {
        Logger.error('Error Occured in updateTierStatus {0}', error.message);
        Logger.error('Error Occured in updateTierStatus {0}', error.stack);
        if (profileIterator instanceof SeekableIterator) profileIterator.close();
        return new Status(Status.ERROR, null, error.message);
    }
}

module.exports = {
    execute: execute
};

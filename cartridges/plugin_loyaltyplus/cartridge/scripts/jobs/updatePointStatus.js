'use strict';

var Transaction = require('dw/system/Transaction');
var Status = require('dw/system/Status');
var SeekableIterator = require('dw/util/SeekableIterator');
var OrderMgr = require('dw/order/OrderMgr');
var CustomerMgr = require('dw/customer/CustomerMgr');
var Logger = require('dw/system/Logger').getLogger('LoyaltyPlusLog', 'LoyaltyPlus');
var system = require('dw/system');
var site = system.Site.getCurrent();

/**
 * @param {number} updatedTierOrderAmount customer's tier spend amount till date
 * @param {Object} loyaltyTier details of all the tier
 * @param {Object} currentTier current tier of the user
 * @returns {string} an string which contains updated loyalty tier information based on amount
 */
function getUpdatedLoyaltyTier(updatedTierOrderAmount, loyaltyTier, currentTier) {
    var updatedLoyaltyTier = currentTier;
    var loyaltyHelper = require('*/cartridge/scripts/helpers/loyaltyHelpers');
    var currentTierDetails = loyaltyHelper.getTierDetails(currentTier, loyaltyTier)
    if (currentTierDetails.max && updatedTierOrderAmount > currentTierDetails.max) {
        Object.keys(loyaltyTier).forEach(function (key) {
            if (!loyaltyTier[key].max) {
                if (updatedTierOrderAmount >= loyaltyTier[key].min) {
                    updatedLoyaltyTier = key;
                }
            } else if (updatedTierOrderAmount >= loyaltyTier[key].min && updatedTierOrderAmount <= loyaltyTier[key].max) {
                updatedLoyaltyTier = key;
            }
        });
    }
    return updatedLoyaltyTier;
}

/**
 * @param {Object} customer customer object
 * @param {Object} order order object
 * @param {string} updatedLoyaltyTier updated loyalty tier
 * @param {string} customerLoyaltyTier current loyalty tier
 * @param {date} refreshDate refresh date
 * @param {number} updatedTotalOrderAmount total amount spent
 * @param {number} updatedTierOrderAmount tier amount spent
 * @param {number} updatedTotalPoints total points details
 * @param {number} customerLoyaltyPendingPoints pending points details
 * @param {number} points points details
 * @returns {boolean} always return true
 */
function updatePoints(customer, order, updatedLoyaltyTier, customerLoyaltyTier, refreshDate, updatedTotalOrderAmount, updatedTierOrderAmount, updatedTotalPoints, customerLoyaltyPendingPoints, points) {
    var customerObj = customer;
    var orderObject = order;
    Transaction.wrap(function () {
        customerObj.profile.custom.loyaltyRefreshDate = refreshDate
        customerObj.profile.custom.loyaltyTierOrderAmount = updatedTierOrderAmount;
        customerObj.profile.custom.loyaltyTier = updatedLoyaltyTier;
        customerObj.profile.custom.loyaltyTotalOrderAmount = updatedTotalOrderAmount;
        customerObj.profile.custom.loyaltyTotalPoints = updatedTotalPoints;
        customerObj.profile.custom.loyaltyPendingPoints = customerLoyaltyPendingPoints - points;
        // update isLoyaltyPointUpdated flag
        orderObject.custom.isLoyaltyPointUpdated = true;
        // update isLoyaltyPointUpdated flag
    });
    return true;
}

/**
 * @param {Object} jobParams - check to see if product feed is enabled.
 * @returns {Status} - The updated status.
 */
function execute(jobParams) {
    // Check the job step is enabled or not
    if (!jobParams.Enabled) {
        return new Status(Status.OK, 'OK', 'Step disabled, skip it...');
    }
    var ordersIterator;
    var updatedTierOrderAmount;
    var refreshDate;
    try {
        var loyaltyHelper = require('*/cartridge/scripts/helpers/loyaltyHelpers');
        var orderStatus = {};
        var notifyCustomer = false;
        orderStatus.ORDER_STATUS_COMPLETED = 5;

        var queryString = 'status = {0} and custom.isLoyaltyOrder = {1} and custom.isLoyaltyPointUpdated = {2}';
        ordersIterator = OrderMgr.queryOrders(queryString, 'orderNo ASC', orderStatus.ORDER_STATUS_COMPLETED, true, false);
        while (ordersIterator.hasNext()) {
            var order = ordersIterator.next();
            if (loyaltyHelper.isOrderFromValidMember(order.getCustomer())) {
                var customer = CustomerMgr.getCustomerByCustomerNumber(
                    order.getCustomer().profile.customerNo
                );
                var loyaltyTier = JSON.parse(site.getCustomPreferenceValue('loyaltyTier'));
                var tierResetPeriod = site.getCustomPreferenceValue('tierResetPeriod');

                var productLineItems = order.getAllProductLineItems();
                var totalNetPrice = 0;
                for (var i = 0; i < productLineItems.length; i++) {
                    var productLineItem = productLineItems[i];
                    totalNetPrice += productLineItem.adjustedPrice.value;
                }
                var points = order.custom.loyaltyOrderPoints;

                var customerLoyaltyPoint = customer.profile.custom.loyaltyTotalPoints;
                var customerLoyaltyOrderAmount = parseFloat(customer.profile.custom.loyaltyTotalOrderAmount);
                var customerLoyaltyTierOrderAmount = parseFloat(customer.profile.custom.loyaltyTierOrderAmount);
                var customerLoyaltyPendingPoints = customer.profile.custom.loyaltyPendingPoints;
                var customerLoyaltyTier = customer.profile.custom.loyaltyTier;
                var updatedTotalOrderAmount = (customerLoyaltyOrderAmount + totalNetPrice).toFixed(2);
                var updatedTotalPoints = customerLoyaltyPoint + points;
                var tierOrderAmount = (customerLoyaltyTierOrderAmount + totalNetPrice).toFixed(2);
                var updatedLoyaltyTier = getUpdatedLoyaltyTier(tierOrderAmount, loyaltyTier, customerLoyaltyTier);
                if (updatedLoyaltyTier !== customerLoyaltyTier) {
                    notifyCustomer = true;
                    updatedTierOrderAmount = 0;
                    var currentDate = new Date();
                    refreshDate = new Date(currentDate.setDate(currentDate.getDate() + tierResetPeriod));
                } else {
                    updatedTierOrderAmount = tierOrderAmount;
                    refreshDate = customer.profile.custom.loyaltyRefreshDate
                }
                updatePoints(customer, order, updatedLoyaltyTier, customerLoyaltyTier, refreshDate, updatedTotalOrderAmount, updatedTierOrderAmount, updatedTotalPoints, customerLoyaltyPendingPoints, points);
                if (notifyCustomer) {
                    loyaltyHelper.sendMail(customer.profile);
                }
            }
        }
        if (ordersIterator instanceof SeekableIterator) {
            ordersIterator.close();
        }
        return new Status(Status.OK, null, 'updatePointStatus job finished');
    } catch (error) {
        Logger.error('Error Occured in updatePointStatus {0}', error.message);
        Logger.error('Error Occured in updatePointStatus {0}', error.stack);
        if (ordersIterator instanceof SeekableIterator) ordersIterator.close();
        return new Status(Status.ERROR, null, error.message);
    }
}

module.exports = {
    execute: execute
};

'use strict';

var formValidation = require('base/components/formValidation');

$(document).on('change', '#add-to-loyalty', function () {
    if ($(this).is(':checked')) {
        $('button[name="loyalty-join"').prop('disabled', false);
        $('button[name="loyalty-join-popup"').prop('disabled', false);
        $('#add-to-loyalty-list').prop('checked', true);
    } else {
        $('button[name="loyalty-join"').prop('disabled', true);
        $('button[name="loyalty-join-popup"').prop('disabled', true);
        $('#add-to-loyalty-list').prop('checked', false);
    }
});

$(document).on('change', '#add-to-loyalty-list', function () {
    if ($(this).is(':checked')) {
        $('button[name="loyalty-join-popup"').prop('disabled', false);
        $('button[name="loyalty-join"').prop('disabled', false);
        $('#add-to-loyalty').prop('checked', true);
    } else {
        $('button[name="loyalty-join-popup"').prop('disabled', true);
        $('button[name="loyalty-join"').prop('disabled', true);
        $('#add-to-loyalty').prop('checked', false);
    }
});

$('#loyalty-join').on("click", function() {
    $("#dob-loyalty-join-error").empty();
    $('#dob-loyalty-join').removeClass('is-invalid');
    $('#dob-loyalty-join').val('');
});

$(document).on('click', '#click-here', function () {
    $('button[name="loyalty-join-popup"').prop('disabled', true);
    $('#add-to-loyalty-list').prop('checked', false);
    $("#dob-loyalty-join-error").empty();
    $('#dob-loyalty-join').removeClass('is-invalid');
    $('#dob-loyalty-join').val('');
});

$('#dob-loyalty-join').change(function() {
    $("#dob-loyalty-join-error").empty();
});

// speedometer
var rangeMeter = $('#range');
var rangeClock = $('.meter-clock');
/**
 * Sets the range for loyalty meter
 */
function rangeChange() {
    var rotateClock = rangeMeter.val();
    rangeClock.css('transform', 'rotate(' + (-90 + ((rotateClock * 180) / 100)) + 'deg)');
}
rangeChange();

module.exports = {
    joinloyalty: function () {
        $(document).on('submit', 'form.join-loyalty-profile', function (e) {
            e.preventDefault();
            if ($('input[name="enableloyalty"]').is(':Checked')) {
                $('input[name="enableloyalty"]').val(true);
            } else {
                $('input[name="enableloyalty"]').val(false);
            }
            var form = $(this);
            var url = form.attr('action');
            form.spinner().start();
            var fields = {};
            var fieldName = '';
            if ($('#add-to-loyalty-list').is(":checked") && $('#dob-loyalty-join').val() === '') {
                form.spinner().stop();
                fieldName = $("#dob-loyalty-join").attr("name");
                fields[fieldName] = $("#dob-loyalty-join").data("required-error");
                formValidation(form, { fields: fields });
                return false;
            }
            if (new Date($('#dob-loyalty-join').val()) > new Date()) {
                form.spinner().stop();
                fieldName = $("#dob-loyalty-join").attr("name");
                fields[fieldName] = $("#dob-loyalty-join").data("invalid-error");
                formValidation(form, { fields: fields });
                return false;
            }
            $.ajax({
                type: 'POST',
                url: url,
                data: form.serialize(),
                dataType: 'JSON',
                success: function () {
                    window.location.reload();
                }
            });
            form.spinner().stop();
            return false;
        });
    }
};

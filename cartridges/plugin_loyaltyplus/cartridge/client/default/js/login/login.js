/**
 * @File Name          : login.js
 * @Description        : login.js file extending core functionality of SFRA
 */

'use strict';

var login = require('base/login/login');
var formValidation = require('base/components/formValidation');
var createErrorNotification = require('base/components/errorNotification');


$(document).ready(function () {
    $(document).on('change', '#add-to-loyalty-list', function () {
        if ($(this).is(':checked')) {
            $('.dob').removeClass('d-none');
            $('.dob').addClass('required');
        } else {
            $('.dob').addClass('d-none');
        }
    });
});

/**
 * register the new user
*/
function register() {
    $('form.registration').submit(function (e) {
        var form = $(this);
        e.preventDefault();
        var url = form.attr('action');
        form.spinner().start();
        form.find('#registration-form-dob').attr('value', form.find('#registration-form-dob').val());
        $('form.registration').trigger('login:register', e);
        if ($('#add-to-loyalty-list').is(":checked") && $('#registration-form-dob').val() === '') {
            form.spinner().stop();
            var fields = {};
            var fieldName = $("#registration-form-dob").attr("name");
            fields[fieldName] = $("#registration-form-dob").data("required-error");
            formValidation(form, { fields: fields });
            return false;
        }
        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: form.serialize(),
            success: function (data) {
                form.spinner().stop();
                if (!data.success) {
                    $('form.registration').trigger('login:register:error', data);
                    formValidation(form, data);
                } else if (data.isloyalty && data.loyaltyURL) {
                    $.ajax({
                        type: 'POST',
                        url: data.loyaltyURL,
                        data: {
                            email: data.email,
                            enableloyalty: true,
                            birthday: data.birthday
                        },
                        success: function () {
                            $('form.registration').trigger('login:register:success', data);
                            window.location.href = data.redirectUrl;
                        }
                    });
                } else {
                    $('form.registration').trigger('login:register:success', data);
                    window.location.href = data.redirectUrl;
                }
            },
            error: function (err) {
                if (err.responseJSON.redirectUrl) {
                    window.location.href = err.responseJSON.redirectUrl;
                } else {
                    createErrorNotification($('.error-messaging'), err.responseJSON.errorMessage);
                }

                form.spinner().stop();
            }
        });
        return false;
    });
}
var exportDetails = $.extend({}, login, {
    register: register
});
module.exports = exportDetails;

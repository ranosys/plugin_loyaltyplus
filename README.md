# Loyalty Plus Plugin (SFRA)

This is a repository for the Loyalty Plus Plugin for Storefront Reference Architecture.

Below mentioned functionalities are included in this cartridge :
- "Join Now" button displayed across the platform for user enrollment in the loyalty program
- Real-time updating of loyalty points for actions like purchases, and engagement activities
- Personalized loyalty dashboard for customers to track points balance, transaction history, and spent amount
- Update Point Status job to process points accumulation, redemption requests, and other loyalty-related transactions
- Update Tier Status job to refresh tier based on the amount spent in a particular refresh period

Please refer to [user guide](/documentation/User Guide.pdf) for configuration details.

# Cartridge Path Considerations
The `plugin_loyaltyplus` plugin requires the app_storefront_base cartridge. In your cartridge path, include the cartridges in the following order:
```
plugin_loyaltyplus:app_storefront_base
```
# The latest version

The latest version of SFRA is 7.0.0
The latest version of Plugin is 1.0.0

# Getting Started

1. Clone this repository.

2. Run `npm install` to install all of the local dependencies (SFRA has been tested with Node v18.19 and is recommended)

3. Run `npm run compile:js` from the command line that would compile all client-side JS files. Run `npm run compile:scss` and `npm run compile:fonts` that would do the same for css and fonts.

4. Create `dw.json` file in the root of the project. Providing a [WebDAV access key from BM](https://documentation.b2c.commercecloud.salesforce.com/DOC1/index.jsp?topic=%2Fcom.demandware.dochelp%2Fcontent%2Fb2c_commerce%2Ftopics%2Fadmin%2Fb2c_access_keys_for_business_manager.html) in the `password` field is optional, as you will be prompted if it is not provided.
```json
{
    "hostname": "your-sandbox-hostname.demandware.net",
    "username": "AM username like me.myself@company.com",
    "password": "your_webdav_access_key",
    "code-version": "version_to_upload_to"
}
```

5. Run `npm run uploadCartridge`. It will upload `plugin_loyaltyplus` cartridge to the sandbox you specified in `dw.json` file.

6. Use [site_meta](/site_meta) to zip and import site data on your sandbox.

7. Add the `plugin_loyaltyplus` cartridge to your cartridge path in _Administration >  Sites >  Manage Sites > RefArch - Settings_ (Note: This should already be populated by the sample data in Step 6).

8. You should now be ready to navigate to the functionality on your site.

# NPM scripts
Use the provided NPM scripts to compile and upload changes to your Sandbox.

## Compiling your application

* `npm run compile:scss` - Compiles all .scss files into CSS.
* `npm run compile:js` - Compiles all .js files and aggregates them.
* `npm run compile:fonts` - Copies all needed font files. Usually, this only has to be run once.

 If you are having an issue compiling scss files, try running 'npm rebuild node-sass' from within your local repo.

## Linting your code

`npm run lint` - Execute linting for all JavaScript and SCSS files in the project. You should run this command before committing your code.

## Uploading

`npm run uploadCartridge` - Will upload `app_storefront_base`, `modules` and `bm_app_storefront_base` to the server. Requires a valid `dw.json` file at the root that is configured for the sandbox to upload.

`npm run upload <filepath>` - Will upload a given file to the server. Requires a valid `dw.json` file.
